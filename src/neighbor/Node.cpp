#include "Node.h"

int main (int argc, const char * argv[])
{
	
        addNode("aaa");
        cout << "This is aaa=" << getNode("aaa") << "=" << endl;
        updateNode("aaa");
        cout << "This is aaa after update=" << getNode("aaa") << "=" << endl;
	
	return 0;
}


/** TABLE FUNCTIONS **/

/** Getter for the node table
 */
map<char *, unsigned int> getNodeTable() {
	return nodeTable;
}

/** Creates a new
 */
void emptyNodeTable() {
	nodeTable = map<char *, unsigned int>();
}

/** Gets the node with a specified key
 */
unsigned int getNode(char* key) {
     return nodeTable[key];
}

/** Gets the node with a specified key. If the node is
 *  already in the table, it does not get added.
 *  @return - true if node was not present in the table
 *          - false if the node was not in the table
 */
bool addNode(char* key) {
     if (nodeTable[key] == 0) {
        nodeTable[key] = newNode();
        return true;
     } else {
        // TODO can call update here 
        return false;
     }
}


/** Updates the node already in the table
 */
void updateNode(char * key) {
    unsigned int node = nodeTable[key];
    node = updateArrivalCount(node);
    nodeTable[key] = node;
}




/** NODE FUNCTIONS **/

/** Used to create a new node
 */
unsigned int newNode() {
	return NEW_NODE;
}


/** Increments the arrival count by 1
 */
unsigned int updateArrivalCount(int node) {

	// TODO how do we use the -1 for arrival count
	// For now we clear it
	node = node | (~NEW_NODE);
	
	// Stores if it had arrived previously
	// TODO Maybe we just clear it at this point
        int originalArrival = node & ARRIVAL_COUNT_MASK;
	// Stores how many times it has been missing
	int arrivalCount = node & ARRIVAL_COUNT_MASK;
	// Increments the arrival time
	arrivalCount++;
	
	
	return node | arrivalCount;
}


/** Sets the arival bit to true
 */
unsigned int setArrivalTrue(int node) {
	return ARRIVAL_TRUE_MASK | node;
}

/** Sets the arival bit to FALSE
 */
unsigned int setArrivalFalse(int node) {
	return (~ARRIVAL_TRUE_MASK) | node;
}
