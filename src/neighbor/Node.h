/** Managing nodes with masks
 *
 * A node is one byte and looks like this:
 * 	10000011
 * 	anr-cccc
 *
 * n = to give a node a value, this never changes.
 * 	This helps us differentiate betwee null and a new node
 *
 * r = if the arrival is -1
 * 	Used for brand new nodes
 *
 * a = arrival
 * 	Whether or not the node has arrived
 *
 * - = not used
 *
 * c = arrival count
 * 	How many times we have gone out without a message
 *
 *
 * Masks
 *
 * ARRIVAL_COUNT_MASK 0x47	01000111
 * ARRIVAL_TRUE_MASK 0x80	10000000
 * NEW_NODE 0xE8		11101000
 * 	Since we are using unsinged ints this is the same as setting it to true, -1, 0.
 * 
 */ 	

#include <iostream>
#include <map>

using namespace std;

#define ARRIVAL_COUNT_MASK 0x47
#define ARRIVAL_TRUE_MASK 0x80
#define ARRIVAL_FALSE_MASK 0x7f
#define NEW_NODE 0xE8


// Global variables
map<char *, unsigned int> nodeTable;


// Forward declraraitons
map<char *, unsigned int> getNodeTable();
void emptyNodeTable();
unsigned int getNode(char*);
bool addNode(char*);
void updateNode(char * key);

// Node Functions
unsigned int newNode();
unsigned int updateArrivalCount(int node);
unsigned int setArrivalTrue(int node);
unsigned int setArrivalFalse(int node);
