// Author: Michel Barbeau, February 2004
// Updated: September 12, 2008
//
// File: wlanrecv.cpp
//

#include <netinet/in.h>

#include <sys/socket.h>
#include <features.h>    /* for the glibc version number */
#if __GLIBC__ >= 2 && __GLIBC_MINOR >= 1
#include <netpacket/packet.h>
#include <net/ethernet.h>     /* the L2 protocols */
#else
#include <asm/types.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>   /* The L2 protocols */
#endif

#include <linux/if.h> // for "struct ifreq"
//#include <linux/wireless.h> // for ther wireless extensions
#include <iostream>

#include <errno.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <iostream>
#include "wlan.hpp"
#include <fcntl.h>
#include <unistd.h>
using namespace std;


/**************************GLOBAL VARIABLES******************/
   
// label of device, e.g. "eth0"
char device[] = "ra0"; 
// interface configuration
Ifconfig ifconfig;
 // prototype header
WLANHeader hdr;

/*************************************************************/

/**************************STRUCTS START HERE**************/

char * WLANAddr::wlan2asc(){
   static char str[32];
    
   sprintf(str, "%x:%x:%x:%x:%x:%x", 
      data[0],data[1],data[2],data[3],data[4],data[5]);

   return str;
}

static int hexdigit(char a){
    if (a >= '0' && a <= '9') return(a-'0');
    if (a >= 'a' && a <= 'f') return(a-'a'+10);
    if (a >= 'A' && a <= 'F') return(a-'A'+10);
    return -1;
}

static int sscanf6(char *str, char *format, int *a1, int *a2, int *a3, int *a4, int *a5, int *a6){
    int n;

    *a1 = *a2 = *a3 = *a4 = *a5 = *a6 = 0;
    while ((n=hexdigit(*str))>=0)
        (*a1 = 16*(*a1) + n, str++);
    if (*str++ != ':') return 1;
    while ((n=hexdigit(*str))>=0)
        (*a2 = 16*(*a2) + n, str++);
    if (*str++ != ':') return 2;
    while ((n=hexdigit(*str))>=0)
        (*a3 = 16*(*a3) + n, str++);
    if (*str++ != ':') return 3;
    while ((n=hexdigit(*str))>=0)
        (*a4 = 16*(*a4) + n, str++);
    if (*str++ != ':') return 4;
    while ((n=hexdigit(*str))>=0)
        (*a5 = 16*(*a5) + n, str++);
    if (*str++ != ':') return 5;
    while ((n=hexdigit(*str))>=0)
        (*a6 = 16*(*a6) + n, str++);

    return 6;
}


Outcome WLANAddr::str2wlan(char * s){
   int a[6], i;
     
   // parse the address
   if (sscanf6(s, (char*)"%x:%x:%x:%x:%x:%x", a, a+1, a+2, a+3, a+4, a+5) < 6) 
   {
      return NOK;
   }

   // make sure the value of every component does not exceed on byte
   for (i=0; i < 6; i++) 
   {
      if (a[i] > 0xff) return NOK;
   }

   // assign the result to the "data" data member
   for (i=0; i < 6; i++) 
   {
      data[i] = a[i];
   }

   return OK;
}

/**************************STRUCTS END HERE******************/

Outcome init()
{
   // (1) create device level socket
   // - PF_PACKET : low level packet interface
   // - SOCK_RAW : raw packets including link level header
   // - ETH_P_ALL : all frames will be received
   if ((ifconfig.sockid = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) == -1)
   { 
      printf("cannot open socket: %s\n", strerror(errno));
      return NOK;
   }

   // (2) fetch the interface index
   struct ifreq ifr;
   strcpy(ifr.ifr_name, device);
   if (ioctl(ifconfig.sockid, SIOGIFINDEX, &ifr) < 0)
   {
      printf("failed to fetch ifindex: %s\n", strerror(errno));
      return NOK;
   }
   ifconfig.ifindex = ifr.ifr_ifindex;
   printf("ifindex: %d\n", ifconfig.ifindex);

   // (3) fetch the hardware address
   if (ioctl(ifconfig.sockid, SIOCGIFHWADDR, &ifr) == -1)
   {
      printf("failed to fetch hardware address: %s\n", strerror(errno));
      return NOK;
   }
   memcpy(&ifconfig.hwaddr.data, &ifr.ifr_hwaddr.sa_data, WLAN_ADDR_LEN);
   printf("hwaddr: %s\n", ifconfig.hwaddr.wlan2asc());

   // (4) fetch the MTU
   if (ioctl(ifconfig.sockid, SIOCGIFMTU, &ifr) == -1)
   {
      printf("WLANProtocol, failed to the MTU: %s\n", strerror(errno));
      return NOK;
   }
   ifconfig.mtu = ifr.ifr_mtu;
   printf("MTU: %d\n", ifconfig.mtu);

   // (5) add the promiscuous mode
   struct packet_mreq mr;
   memset(&mr,0,sizeof(mr));
   mr.mr_ifindex = ifconfig.ifindex;
   mr.mr_type =  PACKET_MR_PROMISC;
   if (setsockopt(ifconfig.sockid, SOL_PACKET, PACKET_ADD_MEMBERSHIP, 
      (char *)&mr, sizeof(mr)) < 0) 
   {
      printf("WLANProtocol, failed to add the promiscuous mode: %s\n", 
         strerror(errno)); 
      return NOK;
   }

	

   // (6) bind the socket to the interface (device)
   struct sockaddr_ll sll;
   memset(&sll, 0, sizeof(sll));
   sll.sll_family = AF_PACKET;
   sll.sll_ifindex = ifconfig.ifindex;
   sll.sll_protocol = htons(ETH_P_ALL);

   // (6.5) Set the socket to Non Blocking
   fcntl(ifconfig.sockid, F_SETFL,O_NONBLOCK|FASYNC);


   if (bind(ifconfig.sockid, (struct sockaddr*)&sll, sizeof(sll)) < 0) 
   {
      printf("WLANProtocol, failed to bind the socket: %s\n", strerror(errno)); 
      return NOK;
   }
	
   //Initialize the random variables
   srand( time(NULL) );
	
   return OK;
}




void ProcessNewMessage(char * anAddress){
	printf("Processing new message...\n");
/*	
	map<char *, NeighborRow>::iterator it;
	it = neighborTable.find(anAddress);
	if (it == 0){	
	    NeighborRow newentry(anAddress, true, -1);
	     neighborTable[anAddress] = newentry;
	}

	else{
		map<char *, NeighborRow>::iterator it;

		it = neighborTable.find(anAddress);
		NeighborRow existingEntry = *it;

		// Modify the existing neighbor
		existingEntry.arrival = true;
		existingEntry.lastRecorded = 0;	
	}
*/
}


void UpdateTable(){
/*
    map<char *, NeighborRow>::iterator it;
    for (it= neighborTable.begin(); it != neighborTable.end(); ++it){
    	NeighborRow current = it->second;
	current.lastRecorded++;
	current.arrival = false; 
    }
*/
	printf("Updating Table...\n");
}


// receive data over a socket
void Receive()
{
   // pointer to received data
   unsigned char * buff = new unsigned char[ifconfig.mtu];
   unsigned int i; // frame length
   struct sockaddr_ll from; // source address of the  message
   socklen_t fromlen = sizeof(struct sockaddr_ll); 
   int error;

  
   
   // infinite loop
   while (true)
   {
	UpdateTable();
	
	 long int randomInterval = rand() % 1000 + 1000;
      // (5) loop until a non-empty frame has been received on "device"
      for (int j = 0; j < randomInterval; j++)
      {
	 
         // (6) wait and receive a frame
         fromlen = sizeof(from);
         i = recvfrom(ifconfig.sockid, buff, ifconfig.mtu, 0, 
            (struct sockaddr *) &from, &fromlen);
         if (i == -1)
         {
            //printf("cannot receive data: %s\n", strerror(errno));
            // sleep for 10 milliseconds before re-trying
            usleep(5000);
         }
	 else
	 {
	    printf("Frame Received!\n");
	    char asciiSrcAddr[32], asciiDestAddr[32];
  
	    // casting to the WLAN header format
	    WLANHeader * wlanHdr = (WLANHeader *) buff;

	    char * dest = wlanHdr->destAddr.wlan2asc();
	    strcpy(asciiDestAddr, dest);
	 
	    char * src = wlanHdr->srcAddr.wlan2asc();
	    strcpy(asciiSrcAddr, src);
	
	    ProcessNewMessage(src);
	    printf("wlan:\nBroadcasting to:%s\nReceiving From: %s\n", asciiDestAddr, asciiSrcAddr);
	 }
      }
      

      
   }
}




void shutdown()
{
   printf("shutdown\n");
   // close the socket
   if (ifconfig.sockid != -1) close(ifconfig.sockid);
}

main()
{
   if (init()==OK) 
   {
      Receive();
      shutdown();
   }
}
